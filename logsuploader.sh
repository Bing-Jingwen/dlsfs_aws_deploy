#!/bin/bash

#DEBUG=echo

type=local

ec2ip () {
#check if on ec2
hostname | grep ip
if [ 0 == $? ]
then # on ec2
    type=aws
    LocalIP=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

else
    type=local
    LocalIP="$(dig +short myip.opendns.com @resolver1.opendns.com)"
fi
}

macip () {
type=local
LocalIP=$(ifconfig en0|awk '/inet / {print $2}')
}

RED='\033[0;31m'
NC='\033[0m'

WorkNodeName=$(hostname)
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     ec2ip;;
    Darwin*)    macip;;
    CYGWIN*)    echo "Cygwin";;
    MINGW*)     echo "MinGw";;
    *)          echo "UNKNOWN:${unameOut}"
esac

# always pull newest worknode:ignite from docker hub without caching.
${DEBUG}  docker pull bingzhang/logsuploader:ignite

${DEBUG}  docker run --rm -it -v $(pwd)/${type}_properties/cluster.properties:/usr/src/dlsfs/src/main/config/cluster.properties \
    -v $(pwd)/log4j.properties:/usr/src/dlsfs/log4j.properties \
    -v $1:/usr/src/dlsfs/logs \
    --net=host --name logsuploader \
    -e "LOGPATH=/usr/src/dlsfs/logs" -e "LINES=$2" \
    bingzhang/logsuploader:ignite
