#!/bin/bash

# stop all docker containers and restart rabbitmq

docker stop $(docker ps -a -q) &> /dev/null

docker rm -f $(docker ps -a -q) &> /dev/null

docker-compose up -d

