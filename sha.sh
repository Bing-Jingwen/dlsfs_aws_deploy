#!/bin/bash

REPO=$1

TAG=$2

#REGISTRY=https://registry.hub.docker.com/v2
REGISTRY=https://registry-1.docker.io/v2

URI="$REGISTRY/$REPO/manifests/$TAG"

MANIFEST="`curl -sL -o /dev/null -D- $URI`"
CHALLENGE="`grep "Www-Authenticate" <<<"$MANIFEST"`"


if [[ CHALLENGE ]]; then
    IFS=\" read _ REALM _ SERVICE _ SCOPE _ <<<"$CHALLENGE"
    SCOPE=repository:bingzhang/lockservice:pull,push
    TOKEN="`curl -skL "$REALM?service=$SERVICE&scope=$SCOPE"`"
    IFS=\" read _ _ _ TOKEN _ <<<"$TOKEN"
    #curl -X GET -H 'accept: application/vnd.docker.distribution.manifest.v2+json' -H "Authorization: Bearer $TOKEN" $URI > manifest.json
    curl -sX GET -H 'accept: application/vnd.docker.distribution.manifest.v2+json' -H "Authorization: Bearer $TOKEN" $URI | jq .config.digest | tr -d '"'
fi
