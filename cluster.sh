#!/bin/bash

#DEBUG=echo

linux () {
#check if on ec2
hostname | grep ip
if [ 0 == $? ]
then # on ec2
    type=aws
    localip=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)
else
    type=local
    localip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
fi
igniteserverContainerName=dlsfs-cluster
dockerImage=bingzhang/cluster:ignite
}

mac () {
dockerImage=bingzhang/cluster:ignite
type=local
localip=$(ifconfig en0|awk '/inet / {print $2}')
igniteserverContainerName=dlsfs-cluster
}

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     linux;;
    Darwin*)    mac;;
    CYGWIN*)    echo "Cygwin";;
    MINGW*)     echo "MinGw";;
    *)          echo "UNKNOWN:${unameOut}"
esac

RED='\033[0;31m'
NC='\033[0m'

echo -e "${RED} start $igniteserverContainerName on $type" ${NC}

docker -v 1>/dev/null
if [ 0 != $? ]
then
  ${DEBUG} ./installDocker.sh
else
  echo "installed Docker version: " $(docker -v)
fi

# always pull from docker hub the newest docker image without caching on local disk
 ${DEBUG} docker pull $dockerImage

# check if ignite server already running
ID=$( docker ps -aqf "name=$igniteserverContainerName")
if [ ! -z "$ID" ]
then
   echo -e "${RED} stop $igniteserverContainerName:" $ID ${NC}
   ${DEBUG} docker stop $ID
   ${DEBUG} docker rm $ID
fi

${DEBUG} docker run -d --rm -it -v $(pwd)/${type}_properties/cluster.properties:/usr/src/dlsfs/src/main/config/cluster.properties -v $(pwd)/log4j.properties:/usr/src/dlsfs/log4j.properties --net=host --name $igniteserverContainerName -e "Local_IP=$localip" $dockerImage

${DEBUG} docker-compose up -d
