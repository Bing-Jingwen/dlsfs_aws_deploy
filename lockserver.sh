#!/bin/bash

#DEBUG=echo

NC='\033[0m'

type=local

ec2ip () {
#check if on ec2
hostname | grep ip
if [ 0 == $? ]
then # on ec2
    type=aws
    LocalClusterPublicDNSName=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)
else
    type=local
    LocalClusterPublicDNSName="$(hostname)"
fi
}

macip () {
type=local
LocalClusterPublicDNSName=$(ifconfig en0|awk '/inet / {print $2}')
}

RemoteClusterPublicDNSName=$1

LocalClusterName=$(hostname)

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     ec2ip;;
    Darwin*)    macip;;
    CYGWIN*)    echo "Cygwin";;
    MINGW*)     echo "MinGw";;
    *)          echo "UNKNOWN:${unameOut}"
esac


BLUE='\033[0;34m'
echo -e "${BLUE}local cluster name: " $LocalClusterName 
echo -e "${BLUE}local cluster public dns name: " $LocalClusterPublicDNSName 
echo -e "${BLUE}remtoe cluster public dns name: " $RemoteClusterPublicDNSName${NC}

if [ -z "$1" ]
then
  RED='\033[0;31m'
  echo -e "${RED} please specify the $1 as remote cluster public dns name" ${NC}
  exit 128
fi

${DEBUG} ./server.sh

# always pull newest lockservice from docker hub without caching.
${DEBUG} docker pull bingzhang/lockservice:ignite

# check if lock service already running
lockserviceContainerName=lockservice
ID=$(docker ps -aqf "name=$lockserviceContainerName")
if [ -z "$ID" ]
then
  ${DEBUG} docker run --rm -it -v $(pwd)/${type}_properties/cluster.properties:/usr/src/dlsfs/src/main/config/cluster.properties -v $(pwd)/log4j.properties:/usr/src/dlsfs/log4j.properties --net=host --name lockservice -e "Local_ClusterName=$LocalClusterName" -e "Local_LockService_IP=$LocalClusterPublicDNSName" -e "Remote_LockService_IP=$RemoteClusterPublicDNSName"  bingzhang/lockservice:ignite
else
  echo "lock service running as docker:" $ID
fi

