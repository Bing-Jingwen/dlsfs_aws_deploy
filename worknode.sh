#!/bin/bash

#DEBUG=echo

type=local

ec2ip () {
#check if on ec2
hostname | grep ip
if [ 0 == $? ]
then # on ec2
    type=aws
    WorkNodeIP=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

else
    type=local
    WorkNodeIP="$(dig +short myip.opendns.com @resolver1.opendns.com)"
fi
}

macip () {
type=local
WorkNodeIP=$(ifconfig en0|awk '/inet / {print $2}')
}


RED='\033[0;31m'
NC='\033[0m'

#${DEBUG} ./server.sh

docker -v 1>/dev/null
if [ 0 -ne "$?" ]
then
  ${DEBUG} ./installDocker.sh
else
  echo "installed Docker version: " $(docker -v)
fi

# start rabbitmq
${DEBUG}  docker-compose up -d

WorkNodeName=$(hostname)
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     ec2ip;;
    Darwin*)    macip;;
    CYGWIN*)    echo "Cygwin";;
    MINGW*)     echo "MinGw";;
    *)          echo "UNKNOWN:${unameOut}"
esac

# always pull newest worknode:ignite from docker hub without caching.
${DEBUG}  docker pull bingzhang/worknode:ignite

${DEBUG}  docker run --rm -it -v $(pwd)/${type}_properties/cluster.properties:/usr/src/dlsfs/src/main/config/cluster.properties -v $(pwd)/log4j.properties:/usr/src/dlsfs/log4j.properties --net=host --name worknode -e "WorkerNode_IP=$WorkNodeIP" -e "WorkerName=worknode-$WorkNodeName" bingzhang/worknode:ignite
