#!/bin/bash

#DEBUG=echo

docker -v 1>/dev/null
if [ 0 != $? ]
then
  ./installDocker.sh
else
  echo "installed Docker version: " $(docker -v)
fi

ec2ip () {
#check if on ec2
hostname | grep ip
if [ 0 == $? ]
then # on ec2
    type=aws
    LocalClusterPublicDNSName=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)
else
    type=local
    LocalClusterPublicDNSName="$(hostname)"
fi
}

macip () {
type=local
LocalClusterPublicDNSName=$(ifconfig en0|awk '/inet / {print $2}')
}

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     ec2ip;;
    Darwin*)    macip;;
    CYGWIN*)    echo "Cygwin";;
    MINGW*)     echo "MinGw";;
    *)          echo "UNKNOWN:${unameOut}"
esac


# always pull newest worknode:ignite from docker hub without caching.
${DEBUG} docker pull bingzhang/simpleclient:ignite

${DEBUG} docker run --rm -it -v $(pwd)/${type}_properties/cluster.properties:/usr/src/dlsfs/src/main/config/cluster.properties -v $(pwd)/log4j.properties:/usr/src/dlsfs/log4j.properties --net=host --name simpleclient bingzhang/simpleclient:ignite
